class CardFacade extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: this.props.cards,
      archiveView: this.props.archiveView,
      listClass: this.props.listView
    };
    this.removeCard = this.removeCard.bind(this);
    this.archiveCard = this.archiveCard.bind(this);
  }

  componentWillReceiveProps(prevProps, prevState, snapshot) {
    console.log(prevProps);
    this.setState({
      archiveView: prevProps.archiveView,
      listClass: prevProps.listView
    });
  }

  archiveCard(e) {
    let cards = this.state.cards;
    cards.forEach(function (card, index) {
      if (Number(index) === Number(e.target.getAttribute('data-index'))) {
        return card.archive = true;
      }
    });
    this.setState({ cards: cards });
    return localStorage.setItem('cards', JSON.stringify(this.state.cards));
  }

  removeCard(e) {
    let cards = this.state.cards;
    cards.splice(e.target.getAttribute('data-index') ,1);
    this.setState({ cards: cards });
    return localStorage.setItem('cards', JSON.stringify(this.state.cards));
  }

  render() {
    function getIcon(category) {
      switch(category) {
        case 'Back End':
          return 'icon-server';
        case 'Database':
          return 'icon-database';
        case 'Design':
          return 'icon-crop';
        case 'Dev Ops':
          return 'icon-hard-drive';
        default:
          return 'icon-monitor';
      }
    }

    function listCard(state) {
      return state ? 'card card-list' : 'card';
    }

    let self = this;

    return(
      <div className="container container-card">
        {this.state.cards.map(function (card, index) {
          if (card.archive === self.state.archiveView || self.state.archiveView === 'all') {
            return <div className={listCard(self.state.listClass)} key={index}>
              <div className="tooltip">
                <i className={getIcon(card.category)}> </i>
                <span className="tooltiptext">{card.category}</span>
              </div>
              <h4>{card.title}</h4>
              <p>{card.description}</p>
              <p className="tags">Tags</p>
              <ul>
                {card.tags.map(function (tag, index) {
                  return <li key={index}>{tag.name}</li>;
                })}
              </ul>
              <div className="row row-icons">
                <i className="icon-trash" data-index={index} onClick={self.removeCard}> </i>
                <i className="icon-archive" data-index={index} onClick={self.archiveCard}> </i>
              </div>
            </div>;
          }
        })}
      </div>
    );
  }
}
