class Main extends React.Component {
  constructor(props) {
    super(props);
    let cards = localStorage.getItem('cards') ? JSON.parse(localStorage.getItem('cards')) : [];
    this.state = {
      archiveView: false,
      cards: cards,
      listView: false
    };
    this.addCard = this.addCard.bind(this);
    this.hideListView = this.hideListView.bind(this);
    this.showListView = this.showListView.bind(this);
    this.showArchive = this.showArchive.bind(this);
    this.showTasks = this.showTasks.bind(this);
  }

  addCard(card) {
    let cards = this.state.cards;
    cards.push(card);
    this.setState({cards: cards});
    return localStorage.setItem('cards', JSON.stringify(this.state.cards));
  }

  hideListView() {
    this.setState({
      listView: false
    });
  }

  showListView() {
    this.setState({
      listView: true
    });
  }

  showArchive() {
    this.setState({
      archiveView: true,
      listView: false
    });
  }

  showTasks() {
    this.setState({
      archiveView: false,
      listView: false
    });
  }

  render() {
    return(
      <div>
        <NavBar
          showList={this.showListView}
          showTasks={this.showTasks}
          showArchive={this.showArchive}
          archiveView={this.state.archiveView} />
        <CardFacade
          cards={this.state.cards}
          archiveView={this.state.archiveView}
          listView={this.state.listView} />
        <ToolBar add={this.addCard} />
      </div>
    );
  }
}
