class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'WrkHorse',
      archiveView: this.props.archiveView
    };
    this.showArchive = this.props.showArchive.bind(this);
    this.archive = this.archive.bind(this);
    this.showTasks = this.props.showTasks.bind(this);
    this.tasks = this.tasks.bind(this);
    this.showList = this.props.showList.bind(this);
    this.list = this.list.bind(this);
  }

  archive() {
    return this.showArchive();
  }

  list() {
    return this.showList();
  }

  tasks() {
    return this.showTasks();
  }

  render() {
    return(
      <div className="navbar">
        <h4>{this.state.title}</h4>
        <ul>
          <li><i className="icon-grid" onClick={this.tasks}> </i></li>
          <li><i className="icon-list" onClick={this.list}> </i></li>
          <li><i className="icon-archive" onClick={this.archive}> </i></li>
          <li><i className="icon-filter"> </i></li>
        </ul>
      </div>
    );
  }
}