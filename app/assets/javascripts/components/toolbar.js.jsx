class ToolBar extends React.Component {
  constructor(props) {
    super(props);
    this.addClass = 'toolbar-add';
    this.title = 'Add New Task';
    this.tags = [];
    this.state = {
      archive: false,
      categories: [
        {name: 'Back End'},
        {name: 'Database'},
        {name: 'Design'},
        {name: 'Dev Ops'},
        {name: 'Front End '}
      ],
      description: '',
      modalClass: 'modal-hidden',
      selectedCategory: 'select',
      tags: this.tags,
      tagValue: '',
      title: '',
      toolbarClass: 'hidden'
    };
    this.changeTag = this.changeTag.bind(this);
    this.addTag = this.addTag.bind(this);
    this.deleteTag = this.deleteTag.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.showModal = this.showModal.bind(this);
    this.addCard= this.addCard.bind(this);
    this.add = this.props.add.bind(this);
  }

  addCard() {
    if (this.hasContent()) {
      this.add({
        archive: false,
        category: this.state.selectedCategory,
        title: this.state.title,
        description: this.state.description,
        tags: this.state.tags,
        create_at: new Date().getTime()
      });
      return this.reset()
    }
  }

  hasContent() {
    return this.state.selectedCategory !== 'select' &&
      this.state.title !== '' &&
      this.state.description !== '';
  }

  addTag() {
    if (!this.testTag(this.state.tagValue) && this.state.tagValue !== '') {
      this.tags.push({name: this.state.tagValue});
      return this.setState({
        tags: this.tags,
        tagValue: ''
      });
    }
  }

  changeTag(e) {
    return this.setState({ tagValue: e.target.value });
  }

  deleteTag() {
    this.tags.pop();
    return this.setState({
      tags: this.tags
    });
  }

  hideModal() {
    this.setState({ modalClass: 'modal-hidden' });
    return this.reset();
  }

  reset() {
    this.setState({
      archive: false,
      categories: [
        {name: 'Back End'},
        {name: 'Database'},
        {name: 'Design'},
        {name: 'Dev Ops'},
        {name: 'Front End '}
      ],
      description: '',
      modalClass: 'modal-hidden',
      selectedCategory: 'select',
      tags: [],
      tagValue: '',
      title: '',
      toolbarClass: 'hidden'
    });
    this.tags = [];
  }

  showModal() {
    return this.setState({ modalClass: 'modal-visible', toolbarClass: 'visible' });
  }

  testTag(newTag) {
    return this.tags.find(function (tag) {
      return tag.name === newTag;
    });
  }

  render() {
    return (
      <div className={this.state.toolbarClass + ' toolbar-container'}>
        <div className={this.state.modalClass + ' modal'}>
          <div className="modal-overall" onClick={this.hideModal}> </div>
          <div className="modal-container">
            <header>
              <h4>{this.title}</h4>
            </header>
            <main>
              <select value={this.state.selectedCategory} onChange={(e)=>{this.setState({selectedCategory: e.target.value})}}>
                <option value="select" disabled>Select Category</option>
                {this.state.categories.map(function (category) {
                  return <option key={category.name}>{category.name}</option>;
                })}
              </select>
              <input type="text" name="title" placeholder="Title" value={this.state.title} onChange={(e)=>{this.setState({ title: e.target.value })}} />
              <textarea name="description" placeholder="Description" value={this.state.description} onChange={(e)=>{this.setState({ description: e.target.value })}}> </textarea>
              <input type="text" placeholder="Tag" value={this.state.tagValue} onChange={this.changeTag} />
              <div className="btn-row">
                <button onClick={this.addTag}>Add Tag</button>
                <button onClick={this.deleteTag}>Delete Tag</button>
              </div>
              <ul>
                {this.state.tags.map(function (tag, index) {
                  return <li key={index}>{tag.name}</li>;
                })}
              </ul>
            </main>
            <footer>
              <button onClick={this.hideModal}>Cancel</button>
              <button onClick={this.addCard}>Save</button>
            </footer>
          </div>
        </div>
        <div className={this.addClass + ' toolbar'} onClick={this.showModal}>
          <i className="icon-plus"> </i>
        </div>
      </div>
    );
  }
}