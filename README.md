# README

Project Management Application build to track development tasks

## Tech Stack

* Ruby
* Rails
* React (react-rails)


## Starting server

```
rails s
```



![screen_shot](https://bitbucket.org/repo/ngqMyEe/images/1202340029-Screen%20Shot%202019-05-17%20at%2010.14.32%20PM.png)
